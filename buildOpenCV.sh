#!/bin/bash

if [ $# -ne 2 ]; then
    echo "Chyba: Skript očekává 2 parametry: ABI pro které má být knihovna sestavena a adresář se sestavenou knihovnou FFmpeg."
    exit 1
fi

if [ -z "$ANDROID_PLATFORM" ]; then
    echo "error: undefined NDK_PLATFORM_LEVEL"
    exit 1
fi

CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

################################################################
### Konfigurační konstanty #####################################
################################################################

# Umístení NDK
NDK_PATH="$HOME/Programming/AndroidNDK/android-ndk-r14b"
ANDROID_SDK="$HOME/Programming/AndroidSDK"

# Adresář, kam se budou stahovat zdrojové soubory knihoven.
SOURCE_DIR=$CURRENT_DIR/sources

# Adresář, kam se uloží sestavené knihovny.
BUILD_DIR=$CURRENT_DIR/build/opencv/android-$1

# Aktuální adresář.
BUILDER_HOME=$(pwd)

if [ $1 == "armeabi-v7a" ]; then
    ANDROID_TOOLCHAIN_NAME=arm-linux-androideabi-4.9
elif [ $1 == "x86" ]; then
    ANDROID_TOOLCHAIN_NAME=x86-4.9
else
    echo "Chyba: Nepodporované ABI ($1)."
    exit 1
fi


# Cesta k sestavené knihovně FFmpeg. 
FFMPEG_BUILD=$2/android-$1

export LD_LIBRARY_PATH=$FFMPEG_BUILD/lib
export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:$FFMPEG_BUILD/lib/pkgconfig
export PKG_CONFIG_LIBDIR=$PKG_CONFIG_LIBDIR:$FFMPEG_BUILD/lib



# Odstraňování přechozích sestavení.
echo "Odstraňuji všechny binární soubory..."
rm -rf $BUILD_DIR



################################################################
### Sestavování knihovny #######################################
################################################################
mkdir -p $SOURCE_DIR
cd $SOURCE_DIR

if [ ! -d "opencv" ]; then
  git clone https://github.com/opencv/opencv
fi

if [ ! -d "opencv_contrib" ]; then
  git clone https://github.com/opencv/opencv_contrib
fi

git -C opencv_contrib reset --hard ced5aa760688dd2ec867ebf7bd4f0c2341d2fde5

cd opencv
git reset --hard 6ffc48769ac60d53c4bd1913eac15117c9b1c9f7
git apply ../../opencv-patch.diff

OPENCV_SOURCE=$(pwd)


mkdir -p $BUILD_DIR
cd $BUILD_DIR
git clean -d -f -x

cmake -DANDROID_TOOLCHAIN_NAME=$ANDROID_TOOLCHAIN_NAME -DANDROID_ABI=$1 -DANDROID_NATIVE_API_LEVEL=$ANDROID_PLATFORM -DCMAKE_TOOLCHAIN_FILE=$NDK_PATH/build/cmake/android.toolchain.cmake -DBUILD_opencv_world=ON -DWITH_FFMPEG=ON -DCMAKE_BUILD_TYPE=Release -DBUILD_PERF_TESTS=OFF -DBUILD_TESTS=OFF -DBUILD_EXAMPLES=OFF -DBUILD_DOCS=OFF -DBUILD_SHARED_LIBS=ON -DOPENCV_EXTRA_MODULES_PATH=$SOURCE_DIR/opencv_contrib/modules $OPENCV_SOURCE

make clean
make -j $(nproc --all)
make install

rm -rf $SOURCE_DIR