# OpenCV for Android

Repozitář obsahuje skript pro sestavení knihovny OpenCV (+ contrib a podpora FFmpeg) pro operační systém Android.

Při spouštění skriptu je třeba mít zavedenou proměnnou ANDROID_PLATFORM, která představuje
verzi platformy.

V záhlaví skriptu je třeba nastavi umístění SDK a NDK. Úspěšně je otestováno NDK verze 18b.

Skript očekává dva parametry, kterými jsou cílové ABI (armeabi-v7a | x86) a cesta k binárním souborům
knihovny FFmpeg, které budou automaticky přilinkovány.

Zdroje budou staženy automaticky a po sestavení binárních souborů budou automaticky odstraněny.
Vytvořené binární soubory knihoven budou na konci sestavování uloženy do adresáře build/opencv/android-$ABI.